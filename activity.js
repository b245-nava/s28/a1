// 1. Created activity.js
// 2. Created database named hotel

// 3. Insert one room
db.hotel.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities.",
	rooms_available: 10,
	isAvailable: false
});

// 4. Insert multiple rooms at once

db.hotel.insertMany([{
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation.",
	rooms_available: 5,
	isAvailable: false
}, {
	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen-sized bed perfect for a simple getaway.",
	rooms_available: 15,
	isAvailable: false
}]);


// 5. Find method to search for a room with the name "double"
db.hotel.find({name: "double"});

// 6. Update queen room's rooms available to 0
db.hotel.updateOne({
	name: "queen"
}, {$set: 
{rooms_available: 0}});

// 7. Deleting all rooms with 0 rooms available
db.hotel.deleteMany({rooms_available: 0});



